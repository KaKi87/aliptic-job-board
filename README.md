# ALIPTIC Job Board

Filtering & RSS feed for the [ALIPTIC job board](https://www.aliptic.net/jobboard.html), built with Fastify on NodeJS & VueJS.

| ![](./screenshots/1.png) | ![](./screenshots/2.png) |
|--------------------------|--------------------------|