import Fastify from 'fastify';
import FastifyVite from '@fastify/vite';
import { renderToString } from 'vue/server-renderer';
import axios from 'axios';
import { Window } from 'happy-dom';
import dayjs from 'dayjs';
import dayjsCustomParseFormat from 'dayjs/plugin/customParseFormat.js';
import RSS from 'rss';

dayjs.extend(dayjsCustomParseFormat);

import {
    hostname,
    port
} from './config.js';

import packageJson from './package.json' assert { type: 'json' };

const getJobs = async () => {
    const window = new Window();
    window.document.documentElement.outerHTML = (await axios('https://www.aliptic.net/jobboard.html')).data;
    return [...window.document.querySelectorAll('[data-category]')].map(element => {
        const linkElement = element.querySelector('h6 a');
        return {
            category: element.getAttribute('data-category'),
            title: linkElement.textContent,
            url: `https://www.aliptic.net/${linkElement.href}`,
            tags: [...element.querySelectorAll('.teaser li')].map(tagElement => tagElement.textContent.trim()),
            publicationDate: dayjs(element.querySelector('time').textContent, 'DD-MM-YYYY')
        };
    });
};

export const main = async isDev => {
    const server = Fastify();

    await server.register(FastifyVite, {
        root: import.meta.url,
        dev: isDev || process.argv.includes('--dev'),
        createRenderFunction: async ({
            createApp
        }) => async () => ({
            element: await renderToString(createApp())
        })
    });

    server.setErrorHandler((error, request, reply) => {
        console.error(error);
        reply.send(error);
    });

    await server.vite.ready();

    server.get(
        '/',
        async (
            request,
            reply
        ) => {
            reply.html(await reply.render());
        }
    );

    server.get(
        '/jobs',
        async (
            request,
            reply
        ) => {
            reply.send(await getJobs());
        }
    );

    server.get(
        '/jobs.rss',
        async (
            request,
            reply
        ) => {
            const
                rss = new RSS({
                    'title': 'ALIPTIC Job Board',
                    'description': `Offres d'emploi de l'Association Limousine des Professionnels des Technologies de l'Information et de la Communication`,
                    'feed_url': `https://${hostname}/jobs.rss`,
                    'site_url': `https://${hostname}`,
                    'image_url': 'https://www.aliptic.net/files/content/logo-ALIPTIC.png',
                    'webMaster': packageJson.author,
                    'copyright': 'ALIPTIC <contact@aliptic.net>',
                    'language': 'fr-FR'
                }),
                jobs = await getJobs();
            for(const job of jobs){
                rss.item({
                    'title': `[${job.category}] ${job.title}`,
                    'description': job.tags.join('\n'),
                    'url': job.url,
                    'date': job.publicationDate.toDate().toUTCString()
                });
            }
            reply
                .type('text/xml')
                .send(rss.xml());
        }
    );

    return server;
}

if(process.argv[1] === new URL(import.meta.url).pathname){
    const server = await main();
    await server.listen({ port });
    console.log(`Listening to port ${port}`);
}
