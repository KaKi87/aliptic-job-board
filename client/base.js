import { createSSRApp } from 'vue';
import base from './base.vue';

export const createApp = () => createSSRApp(base)
