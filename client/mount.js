import { createApp } from './base.js';

createApp(window.hydration).mount(document.body);
