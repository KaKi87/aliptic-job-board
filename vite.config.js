import { resolve, dirname } from 'node:path';
import vuePlugin from '@vitejs/plugin-vue';

export default {
    root: resolve(dirname(new URL(import.meta.url).pathname), 'client'),
    plugins: [
        vuePlugin()
    ]
};
